# AirFarm

## Genesis hackathon project

**Project :** AirFarm, solving farmers problems assisting them with automated drone service on their farmland bringing technology to solve common and extreme use-cases.

**Description:** There is a huge gap of information that farmers in India face when it comes to the awareness about their land, medical and crop assistance.

We at AirFarm is all about solving these problem to bridge the gap between farmer and connecting them with useful technology such as blockchain.

We are in the process of creating autonomous drone monitored AirFarm that can engage farmers & help them to monitor crop growth, Monitor land usage, Get Instant medical assistance for emergency in farms and helping farmer with vertical farming best practice with drone. (To grow more in less space with drone monitoring services.)

Our system is well enough to engage, SMART yet SIMPLE conversation with farmers for proper seed usage, proper fertiliser usage & help in right land acquisition where growth rate is expected to be high with our drone service. 

Also in case of a medical emergency our drone can help any farmer (mostly in a distant place with no medical support nearby) by assisting them with simple instructions with our medical kit installed on drone can help target with emergency needs.

If medical kit doesn't solve target emergency, It directly connect target with the nearest medical professional for such emergency by directly communication via drone. Proper alert can be send to the nearest AirFarm support service centers.

AirFarm provides complete solution for daily farmer’s need.
